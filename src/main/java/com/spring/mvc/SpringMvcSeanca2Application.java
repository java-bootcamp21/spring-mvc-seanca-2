package com.spring.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcSeanca2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringMvcSeanca2Application.class, args);
    }

}
