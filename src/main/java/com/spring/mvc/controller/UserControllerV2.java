package com.spring.mvc.controller;

import com.spring.mvc.model.User;
import com.spring.mvc.service.UserService;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@Validated
@Controller
@RequestMapping("/v2/customer")
public class UserControllerV2 {
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	public String getUserList(Model model) {
		model.addAttribute("customers",userService.getUsers());
		return "html-plain/customer-list-plain";
	}
	
	@GetMapping("/register-view")
	public String getRegisterView(Model model, 
			@RequestParam(name="userId",required = false) Long userId){
		if(userId==null) {
			model.addAttribute("titleView","Customer Registration");
			model.addAttribute("customerForm",new User());
		}else {
			model.addAttribute("titleView","Customer Update");
			model.addAttribute("customerForm",userService.getUserById(userId));
		}

		return "html-plain/registration-form-plain";
	}
	
	@PostMapping("/register")
	public String register(@Valid @ModelAttribute("customerForm") User user,
			BindingResult result) {
		
		if(result.hasErrors()) {
			return "html-plain/registration-form-plain";
		}
		
		if(user.getId()==null) {
			userService.createUser(user);
		}else {
			userService.updateById(user.getId(), user);
		}
		return "redirect:/v2/customer";
	}
	
	@GetMapping("/delete")
	public String deleteUser(@RequestParam("userId") Long id) {
		userService.deleteById(id);
		return "redirect:/v2/customer";
	}

}
